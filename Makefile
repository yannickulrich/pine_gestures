UNAME := $(shell uname -m)
ifeq ($(UNAME), aarch64)
	CC=gcc
else
	CC=aarch64-linux-gnu-gcc
endif

CFLAGS=-Wall

all: pine_gestures toggleflash

pine_gestures: pine_gestures.c
	$(CC) $(CFLAGS) -o $@ $<

toggleflash: toggleflash.c
	$(CC) $(CFLAGS) -o $@ $<

install: pine_gestures toggleflash
	install -Dm755 pine_gestures $(prefix)/usr/bin/pine_gestures
	install -Dm755 toggleflash $(prefix)/usr/bin/toggleflash

install-rc: install
	install -Dm755 gestures $(prefix)/etc/init.d/gestures

install-sysd: install
	install -Dm644 gestures.service $(prefix)/etc/systemd/system/gestures.service


uninstall:
	rm $(prefix)/usr/bin/pine_gestures
	rm $(prefix)/usr/bin/toggleflash

uninstall-rc: uninstall
	rm $(prefix)/etc/init.d/gestures

uninstall-sysd: uninstall
	rm $(prefix)/etc/systemd/system/gestures.service


clean:
	rm pine_gestures
	rm toggleflash
