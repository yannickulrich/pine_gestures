Pine_Gestures
==============================

A simple application for the pinephone to launch applications using
IMU gestures. Originally developed by [Zachary
Schroeder](https://github.com/zschroeder6212/pine_gestures) and
refactored by @yannickulrich.

For example, this allows you to shake your phone to activate the
torch or to twist it to activate the camera.

# Installing
It's probably easiest to install `gcc` and `make` on the phone until I
figure the CI out. On a openrc system such as [postmarketOS](https://gitlab.com/postmarketOS)
```shell
$ make install-rc
```
and on a systemd system
```shell
$ make install-sysd
```

# Usage
```
Usage: pine_gestures [-s <cmd>] [-t <cmd>] [-n <N>] [-S <threshold>] [-T <threshold>] [-v] [-r]
        -s  path to cmd to execute on shake
        -t  path to cmd to execute on twist
        -n  number of movements required, default: 2
        -S  shaking activation threshold, default: 32000
        -T  twisting activation threshold, default: 15000
        -v  verbose
        -r  show raw sensor output (very verbose)
```
