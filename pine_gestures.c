/*
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * Author: Zachary Schroeder
 * Created: July 28 2020
 *
 * Author: Yannick Ulrich
 * Modified: February 19 2022
 */

#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <sys/wait.h>
#include <fcntl.h>
#include <stdlib.h>
#include <poll.h>
#include <linux/input.h>
#include <dirent.h>

//reads file and converts to int
int ftoi(char* path)
{
    FILE *f = fopen(path, "r");
    char buffer[10];

    if(f == NULL)
        return 0;

    if(fgets(buffer, 10, f) == NULL)
        return 0;

    fclose(f);

    return atoi(buffer);
}

char *pathjoin(char*p1, char*p2)
{
    char *newPath = malloc(strlen(p1) + strlen(p2) + 2);
    sprintf(newPath, "%s/%s", p1, p2);
    return newPath;
}

//find file containing string in dir
char* find(char* path, char* contains)
{
    DIR *dir;
    struct dirent *ent;
    char *newPath;

    if ((dir = opendir (path)) != NULL) {

        while ((ent = readdir (dir)) != NULL) {
            if(strstr(ent->d_name, contains) != NULL)
            {
                newPath = pathjoin(path, ent->d_name);
                closedir (dir);
                return newPath;
            }
        }
        closedir (dir);
    }
    return NULL;
}

int open_event_dev(const char* name_needle, int flags)
{
    char path[256];
    char name[256];
    int fd, ret, i;

    // find the right device and open it
    for (i = 0; i < 10; i++) {
        snprintf(path, sizeof(path), "/dev/input/event%d", i);
        fd = open(path, flags);
        if (fd < 0)
            continue;

        ret = ioctl(fd, EVIOCGNAME(256), name);
        if (ret < 0)
            continue;

        if (strstr(name, name_needle))
            return fd;

        close(fd);
    }

    return -1;
}




//vibrate code adapted from https://git.sr.ht/~mil/sxmo-utils/tree/master/programs/sxmo_vibratepine.c
void vibrate(int durationMs, int strength)
{

    int fd, ret;
    int effects;
    struct ff_effect e;
    struct input_event play;

    fd = open_event_dev("vibrator", O_RDWR | O_CLOEXEC);
    if(fd < 0)
    {
        fprintf(stderr, "Can't open vibrator event device");
        exit(1);
    }

    ret = ioctl(fd, EVIOCGEFFECTS, &effects);
    if(ret < 0)
    {
        fprintf(stderr, "EVIOCGEFFECTS failed");
        exit(1);
    }

    e.type = FF_RUMBLE;
    e.id = -1;
    e.u.rumble.strong_magnitude = strength;

    ret = ioctl(fd, EVIOCSFF, &e);
    if(ret < 0)
    {
        fprintf(stderr, "EVIOCSFF failed");
        exit(1);
    }

    play.type = EV_FF;
    play.code = e.id;
    play.value = 3;

    ret = write(fd, &play, sizeof play);
    if(ret < 0)
    {
        fprintf(stderr, "write failed");
        exit(1);
    }

    usleep(durationMs * 1000);

    ret = ioctl(fd, EVIOCRMFF, e.id);
    if(ret < 0)
    {
        fprintf(stderr, "EVIOCRMFF failed");
        exit(1);
    }

    close(fd);
}


void usage(char *name)
{
    printf("Usage: %s [-s <cmd>] [-t <cmd>] [-n <N>] [-S <threshold>] [-T <threshold>] [-v] [-r]\n", name);
    printf("\t-s\tpath to cmd to execute on shake\n");
    printf("\t-t\tpath to cmd to execute on twist\n");
    printf("\t-n\tnumber of movements required, default: 2\n");
    printf("\t-S\tshaking activation threshold, default: 32000\n");
    printf("\t-T\ttwisting activation threshold, default: 15000\n");
    printf("\t-v\tverbose\n");
    printf("\t-r\tshow raw sensor output (very verbose)\n");
    exit(1);
}

int main(int argc, char **argv)
{
    signal(SIGCHLD,SIG_IGN);
    char *IMU_path = NULL;
    char *gyro_x_path = NULL;
    char *accel_y_path = NULL;
    char *twist_command = NULL;
    char *shake_command = NULL;

    int last_accel_y;
    struct timeval start, now;
    long mtime, seconds, useconds = 0;
    int c;
    int verbose = 0;

    // number of shakes detected. The phone must be shaken at least
    // twice to enable the flashlight.
    int shakes = 0;
    int twists = 0;
    int gyro_x;
    int accel_y;

    int shake_threshold = 32000;
    int twist_threshold = 15000;
    int nreq = 2;



    while((c = getopt(argc, argv, "vrs:t:S:T:n:")) != -1){
        switch(c)
        {
            case 'v':
                verbose = 1;
                break;
            case 'r':
                verbose = 2;
                break;

            case 'n':
                nreq = atoi(optarg);

            case 'S':
                shake_threshold = atoi(optarg);
                break;
            case 's':
                shake_command = optarg;
                break;

            case 'T':
                twist_threshold = atoi(optarg);
                break;
            case 't':
                twist_command = optarg;
                break;

            case '?':
                usage(argv[0]);
                return 1;

            default:
                usage(argv[0]);
                return 1;
        }
    }
    if(verbose)
    {
        fprintf(stderr, "Shake threshold: %d\nTwist threshold: %d\n",
            shake_threshold, twist_threshold);
        if(shake_command)
            fprintf(stderr, "Shake command: %s\n", shake_command);
        else
            fprintf(stderr, "No shake command\n");

        if(twist_command)
            fprintf(stderr, "Twist command: %s\n", twist_command);
        else
            fprintf(stderr, "No twist command\n");
    }

    //scan for IMU
    IMU_path = find("/sys/bus/i2c/drivers/inv-mpu6050-i2c", "0068");
    if(!IMU_path)
    {
        fprintf(stderr, "could not find IMU path\n");
        return 1;
    }
    if(verbose) fprintf(stderr, "Found IMU module at %s\n", IMU_path);

    IMU_path = find(IMU_path, "iio:device");
    if(!IMU_path)
    {
        fprintf(stderr, "could not find IMU path\n");
        return 1;
    }
    if(verbose) fprintf(stderr, "Found IMU sub-module at %s\n", IMU_path);

    gyro_x_path = pathjoin(IMU_path, "in_anglvel_x_raw");
    accel_y_path = pathjoin(IMU_path, "in_accel_y_raw");

    //the last accel values, initialized to current accel value
    last_accel_y = ftoi(accel_y_path);

    gettimeofday(&start, NULL);


    while(1)
    {
        //update accel value
        gyro_x = ftoi(gyro_x_path);
        accel_y = ftoi(accel_y_path);

        gettimeofday(&now, NULL);

        seconds  = now.tv_sec  - start.tv_sec;
        useconds = now.tv_usec - start.tv_usec;

        mtime = ((seconds) * 1000 + useconds/1000.0) + 0.5;

        //check for shake
        if(verbose == 2)
            fprintf(stderr, "Jerk: %6d Rotation: %6d\n",
                abs(accel_y-last_accel_y), abs(gyro_x));
        if(abs(accel_y-last_accel_y) > shake_threshold)
        {
            gettimeofday(&start, NULL);
            shakes++;
            if(verbose)
                fprintf(stderr, "Detected IMU shake %d\n", shakes);
            usleep(100000);
        }

        if(abs(gyro_x) > twist_threshold)
        {
            gettimeofday(&start, NULL);
            twists++;
            if(verbose)
                fprintf(stderr, "Detected IMU twist %d\n", twists);
            usleep(100000);
        }

        if(shakes >= nreq)
        {
            shakes = 0;
            if(shake_command)
            {
                vibrate(200, 4000);
                if(verbose)
                {
                    fprintf(stderr, "Running %s...", shake_command);
                    fflush(stderr);
                }
                if(fork() == 0)
                {
                    system(shake_command);
                    exit(0);
                }

                // wait 1 second before continuing to avoid multiple
                // detections
                usleep(1000000);
                if(verbose)
                    fprintf(stderr, " release\n");
            }
            else if(verbose)
                fprintf(stderr, "noop\n");
        }

        if(twists >= nreq)
        {
            twists = 0;

            if(twist_command)
            {
                vibrate(200, 4000);
                if(verbose)
                {
                    fprintf(stderr, "Running %s...", twist_command);
                    fflush(stderr);
                }
                if(fork() == 0)
                {
                    system(twist_command);
                    exit(0);
                }
                // wait 1 second before continuing to avoid multiple
                // detections
                usleep(1000000);
                if(verbose)
                    fprintf(stderr, " release\n");
            }
            else if(verbose)
                fprintf(stderr, "noop\n");
        }

        //took too long
        if(mtime > 1000)
        {
            shakes = 0;
            twists = 0;
        }

        last_accel_y = accel_y;
        usleep(25000);
    }
}
