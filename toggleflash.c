/*
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * Author: Zachary Schroeder
 * Created: July 31 2020
 *
 * Author: Yannick Ulrich
 * Modified: February 19 2022
 */


#include <stdio.h>

int main()
{
    FILE *f;
    char *path = "/sys/devices/platform/led-controller/leds/white:flash/brightness";

    char brightness[2];

    f = fopen(path, "r");
    fgets(brightness, 2, f);
    fclose(f);

    if(brightness[0] == '0')
        brightness[0] = '1';
    else if(brightness[0] == '1')
        brightness[0] = '0';

    f = fopen(path, "w");
    fputs(brightness, f);
    fclose(f);
}
